

import 'package:flutter/material.dart';
import 'package:soil_moisture/services/soil_service.dart';
import 'package:dart_amqp/dart_amqp.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class InfoLayout extends StatelessWidget {
  final String title;
  final List<Map<String, dynamic>> sub;
  final Widget ext;
  const InfoLayout ({Key key, this.title, this.sub, this.ext}): super(key: key);
  @override
  Widget build(BuildContext context){
    String subtitle = "";
    for(var item in sub){
      var title = item['title'];
      var values = item['values'];
      // item.map((key, value) => (key=="title"?title=value:values=value));
      subtitle+=title+": "+values+"\n";
    }
    subtitle.substring(0, subtitle.length-3);
    return ListTileTheme(
      child:ListTile(
        title:Text(
          this.title
          ),
        subtitle: Text(subtitle),
        trailing: ext,
      ),
    );
  }
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String data_log = "-";
  String data_pompa = "-";
  String keterangan_soil = "-";
  int value_soil = 0;
  bool sw_soil = false;
  bool sw_pompa = false;
  RMQService service = new RMQService();
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void loadService(){
    //Load data pompa
    service.data("Aktuator").then((str) =>{
      //Ambil consumer Stream dari soil_service.dart
      str.listen((AmqpMessage result) { 
        String message = result.payloadAsString;
        List<String> list = message.split("#");
        int value = int.parse(list[1]);
        var pompa_serial = list[0];
        
        if(data_pompa != pompa_serial){
          data_pompa = pompa_serial;
        }
        setState((){
          if (value == 1) {
            sw_pompa = true;
          } else if (value == 0) {
            sw_pompa = false;
          }else{
            data_log = "Wrong Input Detected";
          }
        });
      })
    });

    service.data("Log").then((str) =>{
      str.listen((AmqpMessage result) { 
        String message = result.payloadAsString;
        List<String> list = message.split("#");
        int value = int.parse(list[1]);
        sw_soil = true;
        if(data_log != list[0]){
          data_log = list[0];
        }
        
        setState((){
          value_soil = value;
          if (value < 350) {
            keterangan_soil = 'lembab';
          } else if (value > 700) {
            keterangan_soil = 'Kering';
          } else {
            keterangan_soil = 'Normal';
          }
        });
        
      })
    });
    // setState(() {
      
    // });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadService();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return MaterialApp(
      title:"Samples",
      home:Scaffold(
        appBar:AppBar(
          title:Text("Samples")
        ),
        body:ListView(
          children: <Widget>[
            InfoLayout(
              title:"Soil Moisture",
              sub:[
                {
                  "title":"Serial Device",
                  "values":"$data_log"
                },
                {
                  "title":"Values",
                  "values":"$value_soil"
                },
                {
                  "title":"Keterangan",
                  "values":"$keterangan_soil"
                }
              ],
              ext:Switch(
                value: sw_soil,
                activeTrackColor: Colors.lightGreenAccent,
                activeColor: Colors.green,
              )
            ),
            InfoLayout(
              title:"Pompa",
              sub:[
                {
                  "title":"Serial Device",
                  "values":"$data_pompa"
                }
              ],
              ext:Switch(
                value: sw_pompa,
                activeTrackColor: Colors.lightGreenAccent,
                activeColor: Colors.green,
              )
            ),
          ]
        ),
      )
    );
  }
}
