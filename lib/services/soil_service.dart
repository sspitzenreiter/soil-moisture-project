import 'package:dart_amqp/dart_amqp.dart';
import 'dart:async';
import 'dart:convert' show json;

import 'package:flutter/services.dart';
import 'package:soil_moisture/model/soil_conn.dart';
class RMQService{
  Future<Consumer> data(String queue) async {
    String connString = await rootBundle.loadString("assets/config/soil_conn.json");
    final jsonData = json.decode(connString);
    SoilConn conn = SoilConn.fromJson(jsonData);
    ConnectionSettings settings = new ConnectionSettings(
      host: conn.hostQueue,
      authProvider: new PlainAuthenticator(conn.userQueue, conn.passQueue),
      virtualHost: conn.VhostQueue,
    );
    Client client = new Client(settings: settings);
    return client
        .channel()
        .then((Channel channel) => channel.queue(queue, durable: true))
        .then((Queue queue) => queue.consume());
  }
}