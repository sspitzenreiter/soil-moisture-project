class SoilConn {
  String userQueue;
  String passQueue;
  String VhostQueue;
  String hostQueue;

  SoilConn({this.userQueue, this.passQueue, this.VhostQueue, this.hostQueue});

  @override
  String toString(){
    return 'SoilConn{userQueue: $userQueue, passQueue: $passQueue, VhostQueue: $VhostQueue, hostQueue: $hostQueue}';
  }

  factory SoilConn.fromJson(Map<String, dynamic> json){
    return SoilConn(
      userQueue: json["userQueue"],
      passQueue: json["passQueue"],
      VhostQueue: json["VhostQueue"],
      hostQueue: json["hostQueue"]
    );
  }

}